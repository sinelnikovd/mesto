$(document).ready(function() {

  $("a[rel='m_PageScroll2id']").mPageScroll2id();

  $('.sec5__slider .owl-carousel').owlCarousel({
    loop: true,
    items: 3,
    dots: false,
    nav: true,
    margin: 35,
    navText: [
    '<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="20px"><path fill-rule="evenodd"  fill="rgb(0, 0, 0)" d="M12.828,17.069 L10.000,19.898 L2.929,12.827 L2.929,12.827 L0.101,9.999 L2.929,7.170 L2.929,7.170 L10.000,0.099 L12.828,2.927 L5.757,9.998 L12.828,17.069 Z"/></svg></span>',
    '<span><svg  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="20px"><path fill-rule="evenodd" d="M12.899,9.999 L10.071,12.827 L10.071,12.827 L3.000,19.898 L0.171,17.069 L7.242,9.998 L0.171,2.927 L3.000,0.099 L10.071,7.170 L10.071,7.170 L12.899,9.999 Z"/></svg></span>'],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        992:{
            items:3
        }
    }
  });

  $('.sec8 .owl-carousel')
  .on('initialized.owl.carousel', function(event) {
    console.log('msg')
    var maxHeight = 0;
    $('.sec8__item').each(function(e) {
      $(this).css('min-height', 'auto');
      maxHeight = ($(this).outerHeight() > maxHeight) ? $(this).outerHeight() : maxHeight;
    });
    $('.sec8__item').css('min-height', maxHeight);
  })
  .on('resized.owl.carousel', function(event) {
    console.log('msg')
    var maxHeight = 0;
    $('.sec8__item').each(function(e) {
      $(this).css('min-height', 'auto');
      maxHeight = ($(this).outerHeight() > maxHeight) ? $(this).outerHeight() : maxHeight;
    });
    $('.sec8__item').css('min-height', maxHeight);
  }).owlCarousel({
    nav: true,
    margin: 30,
     navText: [
    '<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="20px"><path fill-rule="evenodd"  fill="rgb(0, 0, 0)" d="M12.828,17.069 L10.000,19.898 L2.929,12.827 L2.929,12.827 L0.101,9.999 L2.929,7.170 L2.929,7.170 L10.000,0.099 L12.828,2.927 L5.757,9.998 L12.828,17.069 Z"/></svg></span>',
    '<span><svg  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="20px"><path fill-rule="evenodd" d="M12.899,9.999 L10.071,12.827 L10.071,12.827 L3.000,19.898 L0.171,17.069 L7.242,9.998 L0.171,2.927 L3.000,0.099 L10.071,7.170 L10.071,7.170 L12.899,9.999 Z"/></svg></span>'],
    responsive:{
        0:{
          items:1
        },
        500:{
          items: 2
        },
        700:{
          items:3
        },
        992:{
          items:4,
          nav: false
        }
    }
  });

  $('.sec9 .owl-carousel').owlCarousel({
    nav: true,
    margin: 30,
     navText: [
    '<span><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="20px"><path fill-rule="evenodd"  fill="rgb(0, 0, 0)" d="M12.828,17.069 L10.000,19.898 L2.929,12.827 L2.929,12.827 L0.101,9.999 L2.929,7.170 L2.929,7.170 L10.000,0.099 L12.828,2.927 L5.757,9.998 L12.828,17.069 Z"/></svg></span>',
    '<span><svg  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="13px" height="20px"><path fill-rule="evenodd" d="M12.899,9.999 L10.071,12.827 L10.071,12.827 L3.000,19.898 L0.171,17.069 L7.242,9.998 L0.171,2.927 L3.000,0.099 L10.071,7.170 L10.071,7.170 L12.899,9.999 Z"/></svg></span>'],
    responsive:{
        0:{
          items:1
        },
        500:{
          items: 2
        },
        700:{
          items:3
        },
        992:{
          items:4,
          nav: false
        }
    }
  });

  var $slick = $('.sec5__tab-container')
  $slick.slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    draggable: false,
    fade: true,
    adaptiveHeight: true,
    speed: 0,
  })
  $slick.on('afterChange', function(event, slick, currentSlide){
    $('.sec5__tab-nav a.active').removeClass('active');
    $('.sec5__tab-nav a').eq(currentSlide).addClass('active');
  });

  $('.sec5__tab-nav').on('click', 'a:not(.active)' ,function(e) {
    $slick.slick('slickGoTo', $(this).index());
    return false;
  });

  var $slick9 = $('.sec9__tab-container')
  $slick9.slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    draggable: false,
    fade: true,
    adaptiveHeight: true,
    speed: 0,
  });
  $slick9.on('afterChange', function(event, slick, currentSlide){
    $('.sec9__tab-nav a.active').removeClass('active');
    $('.sec9__tab-nav a').eq(currentSlide).addClass('active');
  });

  $('.sec9__tab-nav').on('click', 'a:not(.active)' ,function(e) {
    $slick9.slick('slickGoTo', $(this).index());
    return false;
  });

  $('.js-popup').magnificPopup();
  $('.js-popup-image').magnificPopup({
    type: 'image'
  });

  $(".js-popup-video").magnificPopup({
    type: 'iframe',
    iframe: {
      patterns: {
        youtube: {
          index: 'youtube.com/',
          id: 'v=',
          src: '//www.youtube.com/embed/%id%?autoplay=1'
        },
      },
    }
  });

  $(window).scroll(function() {
    var scrollTop = $(window).scrollTop();
    if(scrollTop > 0) {
      $('body').addClass('fixed-menu')
    }else{
      $('body').removeClass('fixed-menu')
    }

  });

  $("#mobile-menu").mmenu({
    extensions: ['pagedim-black' , 'fx-menu-slide'],
    isMenu: true,
    navbar: {
      add: false,
    },
    offCanvas: {
      position: 'right'
    }
  });

        //   Get the API
  var api = $("#mobile-menu").data( "mmenu" );
  api.bind('open:finish', function () {
    $('#hamburgers').addClass('is-active');
  }).bind('close:start', function () {
    $('#hamburgers').removeClass('is-active');
  });


});